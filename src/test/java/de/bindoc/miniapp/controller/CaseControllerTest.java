package de.bindoc.miniapp.controller;


import de.bindoc.miniapp.data.Case;
import de.bindoc.miniapp.data.CaseRepository;
import de.bindoc.miniapp.data.Doctor;
import de.bindoc.miniapp.data.Patient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class CaseControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CaseRepository caseRepository;

    @Test
    public void getAllCases() throws Exception {
        Mockito.when(caseRepository.findAll()).thenReturn(Arrays.asList(
                new Case(1L, LocalDateTime.of(2018, 2, 1, 12, 0, 0), 60, "Diagnose", new Patient(), new Doctor())
        ));
        mockMvc.perform(get("/cases/"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.[0].start", is("2018-02-01T12:00:00"))) // check if start is formatted in ISO-8601
                .andExpect(jsonPath("$.[0].id", is(1)));
    }

}