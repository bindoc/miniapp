package de.bindoc.miniapp.data;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "_case")
// added prefix "_" for mysql, where case is a key word and hibernate generates
// errenous sql statements
public class Case {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "case_id")
	private Long id;

	private LocalDateTime start;
	private Integer duration;

	@Column(length = 2000)
	private String diagnosis;

	@OneToOne
	@JoinColumn(name = "patient_id")
	private Patient patient;

	@OneToOne
	@JoinColumn(name = "doctor_id")
	private Doctor doctor;

	public Case() {
	}

	public Case(Long id, LocalDateTime start, Integer duration, String diagnosis, Patient patient, Doctor doctor) {
		super();
		this.id = id;
		this.start = start;
		this.duration = duration;
		this.diagnosis = diagnosis;
		this.patient = patient;
		this.doctor = doctor;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getStart() {
		return start;
	}

	public void setStart(LocalDateTime start) {
		this.start = start;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

}
