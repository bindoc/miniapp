package de.bindoc.miniapp;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import de.bindoc.miniapp.data.Address;
import de.bindoc.miniapp.data.AddressRepository;
import de.bindoc.miniapp.data.Case;
import de.bindoc.miniapp.data.CaseRepository;
import de.bindoc.miniapp.data.Doctor;
import de.bindoc.miniapp.data.DoctorRepository;
import de.bindoc.miniapp.data.Patient;
import de.bindoc.miniapp.data.PatientRepository;

@SpringBootApplication
public class MiniApp {

	public static void main(String[] args) {

		SpringApplication.run(MiniApp.class, args);
	}

	@Bean
	public CommandLineRunner demo(AddressRepository addressRepo, DoctorRepository docRepo,
			PatientRepository patientRepo, CaseRepository caseRepo) {
		return (args) -> {
			// create addresses
			Address ad1 = new Address(1L, "Downing Street", 10, "WC2N 5DU", "London");
			Address ad2 = new Address(2L, "Knightsbridge", 15, "WC2N 5DU", "London");
			addressRepo.save(Arrays.asList(ad1, ad2));

			// create patients
			Patient patient1 = new Patient(1L, "Donald", "Duck", LocalDate.of(1960, 3, 25), ad1);
			patientRepo.save(patient1);

			// create doctors
			Doctor doc1 = new Doctor(1L, "Erik", "Wunderlich", ad2, "erwu@hosptial.co.uk", "meinpw");
			docRepo.save(doc1);

			// create case
			caseRepo.save(new Case(1L, LocalDateTime.now(), 5, "Patient has a temper. Prescription of a mild sedative.",
					patient1, doc1));
		};
	}

}
