package de.bindoc.miniapp.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import de.bindoc.miniapp.data.Case;
import de.bindoc.miniapp.data.CaseRepository;
import de.bindoc.miniapp.data.Doctor;
import de.bindoc.miniapp.data.DoctorRepository;
import de.bindoc.miniapp.data.Patient;
import de.bindoc.miniapp.data.PatientRepository;

@Controller
@RequestMapping(path = "/cases")
public class CaseController {

	@Autowired
	private CaseRepository caseRepository;

	@Autowired
	private DoctorRepository doctorRepository;

	@Autowired
	private PatientRepository patientRespository;

	@GetMapping("/")
	public @ResponseBody Iterable<Case> getAllCases() {
		return caseRepository.findAll();
	}

	@GetMapping("/{id}")
	public @ResponseBody Case getCase(@PathVariable("id") Long id) {
		return caseRepository.findOne(id);
	}

	@PostMapping("/")
	public @ResponseBody String addNewCase(@RequestParam String diagnosis, @RequestParam long doctorId,
			@RequestParam int duration, @RequestParam long patientId, @RequestParam String start) {

		Doctor doctor = doctorRepository.findOne(doctorId);
		Patient patient = patientRespository.findOne(patientId);
		LocalDateTime startAsLocalDate = null;
		try {
			startAsLocalDate = LocalDateTime.parse(start, (DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		} catch (DateTimeParseException e) {
		}

		if (doctor == null || patient == null || startAsLocalDate == null) {
			String errorMsg = "";
			if (doctor == null)
				errorMsg += String.format("doctor with id %d not found | ", doctorId);
			if (patient == null)
				errorMsg += String.format("patient with id %d not found | ", patientId);
			if (startAsLocalDate == null)
				errorMsg += String.format("unable to parse %s as start of diagnosis", start);
			return errorMsg;
		}

		Case newCase = new Case(0L, startAsLocalDate, duration, diagnosis, patient, doctor);
		caseRepository.save(newCase);
		return "added";
	}

	@PutMapping("/{id}")
	public @ResponseBody String updateCase(@RequestParam long caseId, @RequestParam String diagnosis,
			@RequestParam long doctorId, @RequestParam int duration, @RequestParam long patientId,
			@RequestParam String start) {

		Case updateCase = caseRepository.findOne(caseId);
		Doctor doctor = doctorRepository.findOne(doctorId);
		Patient patient = patientRespository.findOne(patientId);
		LocalDateTime startAsLocalDate = null;
		try {
			startAsLocalDate = LocalDateTime.parse(start, (DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		} catch (DateTimeParseException e) {
		}

		if (doctor == null || patient == null || startAsLocalDate == null || updateCase == null) {
			String errorMsg = "";
			if (updateCase == null)
				errorMsg += String.format("case with id %d not found", caseId);
			if (doctor == null)
				errorMsg += String.format("doctor with id %d not found | ", doctorId);
			if (patient == null)
				errorMsg += String.format("patient with id %d not found | ", patientId);
			if (startAsLocalDate == null)
				errorMsg += String.format("unable to parse %s as start of diagnosis", start);

			return errorMsg;
		}

		updateCase.setDiagnosis(diagnosis);
		updateCase.setDoctor(doctor);
		updateCase.setDuration(duration);
		updateCase.setPatient(patient);
		updateCase.setStart(startAsLocalDate);
		caseRepository.save(updateCase);
		return "updated";
	}

	@DeleteMapping("/{id}")
	public String deleteCase(@PathVariable Long id) {

		Case caseFound = caseRepository.findOne(id);
		if (caseFound == null)
			return String.format("case with id %d not found", id);

		caseRepository.delete(caseFound);
		return "deleted";
	}

}
