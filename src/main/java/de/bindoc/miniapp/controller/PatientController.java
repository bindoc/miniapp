package de.bindoc.miniapp.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import de.bindoc.miniapp.data.Address;
import de.bindoc.miniapp.data.Patient;
import de.bindoc.miniapp.data.PatientRepository;

@Controller
@RequestMapping(path = "/patients")
public class PatientController {

	@Autowired
	private PatientRepository patientRepository;

	@GetMapping("/")
	public @ResponseBody Iterable<Patient> getAllPatients() {
		return patientRepository.findAll();
	}

	@GetMapping("/{id}")
	public @ResponseBody Patient getPatient(@PathVariable("id") Long id) {
		return patientRepository.findOne(id);
	}

	@PostMapping("/")
	public String addNewPatient(@RequestParam String firstName, @RequestParam String lastName,
			@RequestParam String dateOfBirth, @RequestParam Address address) {

		Patient patient = new Patient();
		patient.setFirstName(firstName);
		patient.setLastName(lastName);
		LocalDate dateOfBirthObject;
		try {
			dateOfBirthObject = LocalDate.parse(dateOfBirth, (DateTimeFormatter.ISO_LOCAL_DATE));
			patient.setDateOfBirth(dateOfBirthObject);
		} catch (DateTimeParseException e) {
			return String.format("unable to parse date of birth format %s", dateOfBirth);
		}
		patient.setAddress(address);
		patientRepository.save(patient);
		return "added";

	}

	@PutMapping("/{id}")
	public String updatePatient(@PathVariable Long id, @RequestBody Patient patient) {

		Patient patientFound = patientRepository.findOne(id);
		if (patientFound == null)
			return String.format("patient with id %d not found | ", id);

		patientRepository.save(patient);
		return "updated";
	}

	@DeleteMapping("/{id}")
	public String deletePatient(@PathVariable Long id) {

		Patient patientFound = patientRepository.findOne(id);
		if (patientFound == null)
			return String.format("patient with id %d not found | ", id);

		patientRepository.delete(id);
		return "deleted";
	}

}
