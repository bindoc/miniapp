package de.bindoc.miniapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import de.bindoc.miniapp.data.Address;
import de.bindoc.miniapp.data.Doctor;
import de.bindoc.miniapp.data.DoctorRepository;

@Controller
@RequestMapping(path = "/doctors")
public class DoctorController {

	@Autowired
	private DoctorRepository doctorRepository;

	@PostMapping("/")
	public @ResponseBody String addNewDoctor(@RequestParam String firstName, @RequestParam String lastName,
			@RequestParam String street, @RequestParam int streetNumber, @RequestParam String postalCode,
			@RequestParam String city, @RequestParam String email, @RequestParam String password) {

		Doctor doctor = new Doctor();
		doctor.setFirstName(firstName);
		doctor.setLastName(lastName);
		Address address = new Address(0L, street, streetNumber, postalCode, city);
		doctor.setAddress(address);
		doctor.setEmail(email);
		doctor.setPassword(password);

		doctorRepository.save(doctor);
		return "added";
	}

	@GetMapping("/")
	public @ResponseBody Iterable<Doctor> getAllDoctors() {
		return doctorRepository.findAll();
	}

	@GetMapping("/{id}")
	public @ResponseBody Doctor getDoctor(@PathVariable("id") Long id) {
		return doctorRepository.findOne(id);
	}

	@PutMapping("/{id}")
	public String updateDoctor(@PathVariable Long id, @RequestBody Doctor doctor) {

		Doctor docFound = doctorRepository.findOne(id);
		if (docFound == null)
			return String.format("doctor with id %d not found | ", id);

		doctorRepository.save(doctor);
		return "updated";
	}

	@DeleteMapping("/{id}")
	public String deleteDoctor(@PathVariable Long id) {

		Doctor docFound = doctorRepository.findOne(id);
		if (docFound == null)
			return String.format("doctor with id %d not found", id);

		doctorRepository.delete(docFound);
		return "deleted";
	}

}